@extends('layouts.app')

@section('title', 'Edit Company')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Edit Company Form') }}</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif
                        <div class="container">
                            <form action="{{ route('company.update', Crypt::encrypt($company->id)) }}" method="post" enctype="multipart/form-data">@csrf {{ method_field('PUT') }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="CompanyName" class="form-label">Name of Company</label>
                                            <input type="text" name="name" id="CompanyName" class="form-control @error('name') is-invalid @enderror" placeholder="Enter name of company" value="{{ $company->name }}" required>
                                            @error('name')
                                            <span class="text-danger is-invalid text-sm-start">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="CompanyEmail" class="form-label">Email of Company</label>
                                            <input type="email" name="email" id="CompanyEmail" class="form-control @error('email') is-invalid @enderror" placeholder="Enter email of company" value="{{ $company->email }}">
                                            @error('email')
                                            <span class="text-danger is-invalid text-sm-start">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="CompanyLogo" class="form-label">Logo of company</label>
                                            <input type="file" name="logo" accept="image/png, image/jpeg, image/svg" id="CompanyLogo" class="form-control @error('logo') is-invalid @enderror" placeholder="Select logo of company">
                                            @error('logo')
                                            <span class="text-danger is-invalid text-sm-start">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="CompanyWebsite" class="form-label">Website of Company</label>
                                            <input type="url" name="website" id="CompanyWebsite" class="form-control @error('website') is-invalid @enderror" placeholder="Enter Website of company" value="{{ $company->website }}">
                                            @error('website')
                                            <span class="text-danger is-invalid text-sm-start">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12 d-flex justify-content-center mt-3">
                                        <button type="submit" class="btn btn-primary">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ route('company.index') }}" class="btn btn-primary">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
