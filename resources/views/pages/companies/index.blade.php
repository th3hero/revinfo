@extends('layouts.app')

@section('title', 'Companies Listing')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Companies List') }}</div>
                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Logo</th>
                                    <th scope="col">Company Name</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($companies as $key=>$company)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td><img src="{!! asset("storage/$company->logo") !!}" alt="{{ $company->name }}" class="img-thumbnail img-fluid" width="50"></td>
                                        <td>{{ $company['name'] }}</td>
                                        <td><a href="{{ route('company.show', Crypt::encrypt($company['id'])) }}" class="btn btn-primary">View</a>&nbsp;&nbsp;&nbsp;<a href="{{ route('company.edit', Crypt::encrypt($company['id'])) }}" class="btn btn-secondary">Edit</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form{{$key}}').submit();">Delete</a><form id="delete-form{{$key}}" method="post" action="{{ route('company.destroy', Crypt::encrypt($company['id'])) }}" class="d-none">@csrf {{ method_field('DELETE') }}</form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">No Records Available!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            {!! $companies->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
