@extends('layouts.app')

@section('title', 'Company Detail')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ $company->name }} Information</div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row mb-3">
                                <div class="col-md-6 d-flex justify-content-center">
                                    <img src="{!! asset("storage/$company->logo") !!}" alt="image">
                                </div>
                                <div class="col-md-6 justify-content-center">
                                    <h3 class="mb-4">Company : <span class="text-dark">{{ $company->name }}</span></h3>
                                    <h5>Email : <a class="text-black" href="mailto:{{ $company->email }}">{{ $company->email }}</a></h5>
                                    <h5>Website : <a class="text-black" href="{{ $company->website }}">{{ $company->website }}</a></h5>
                                </div>
                            </div>
                            <div class="table-responsive mt-5">
                                <h3 class="text-black text-center mt-5">{{ __('List of Employees in This Company!') }}</h3>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($company->Employees as $key=>$employee)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $employee['first_name'].' '.$employee['last_name'] }}</td>
                                            <td>{{ $employee['email'] ?? 'Not Available' }}</td>
                                            <td><a href="{{ route('employee.show', Crypt::encrypt($employee['id'])) }}" class="btn btn-primary">View</a>&nbsp;&nbsp;&nbsp;<a href="{{ route('employee.edit', Crypt::encrypt($employee['id'])) }}" class="btn btn-secondary">Edit</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form{{$key}}').submit();">Delete</a>
                                                <form id="delete-form{{$key}}" method="post" action="{{ route('employee.destroy', Crypt::encrypt($employee['id'])) }}" class="d-none">@csrf {{ method_field('DELETE') }}</form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4" class="text-center">No Records Available!</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
