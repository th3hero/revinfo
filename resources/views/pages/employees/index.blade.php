@extends('layouts.app')

@section('title', 'Employees Listing')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Employees Listing') }}</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Company Name</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($employees as $key=>$employee)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $employee['first_name'].' '.$employee['last_name'] }}</td>
                                        <td>{{ $employee['email'] ?? 'Not Available' }}</td>
                                        <td>{{ $employee->Company->name }}</td>
                                        <td><a href="{{ route('employee.show', Crypt::encrypt($employee['id'])) }}" class="btn btn-primary">View</a>&nbsp;&nbsp;&nbsp;<a href="{{ route('employee.edit', Crypt::encrypt($employee['id'])) }}" class="btn btn-secondary">Edit</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form{{$key}}').submit();">Delete</a>
                                            <form id="delete-form{{$key}}" method="post" action="{{ route('employee.destroy', Crypt::encrypt($employee['id'])) }}" class="d-none">@csrf {{ method_field('DELETE') }}</form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center">No Records Available!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            {!! $employees->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
