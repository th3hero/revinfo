@extends('layouts.app')

@section('title', 'Employee Detail')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Employee View Page') }}</div>

                    <div class="card-body">
                        <div class="container row">
                            <div class="col-md-6">
                                <h4>First Name: <span class="text-black">{{ $employee['first_name'] }}</span></h4>
                            </div>
                            <div class="col-md-6">
                                <h4>Last Name: <span class="text-black">{{ $employee['last_name'] }}</span></h4>
                            </div>
                            <div class="col-md-6 mt-3">
                                <h5>Company: <span class="text-black">{{ $employee->Company->name }}</span></h5>
                            </div>
                            <div class="col-md-6 mt-3">
                                <h5>Email: <span class="text-black">{{ $employee['email'] }}</span></h5>
                                <h5>Phone: <span class="text-black">{{ $employee['phone'] }}</span></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
