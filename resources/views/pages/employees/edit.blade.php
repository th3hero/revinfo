@extends('layouts.app')

@section('title', 'Edit Employee')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Employee Details Editing Form') }}</div>
                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('employee.update', Crypt::encrypt($employee['id'])) }}">@csrf {{ method_field('PUT') }}
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="FirstName" class="form-label">First Name of Employee</label>
                                    <input id="FirstName" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ $employee['first_name'] }}" required autocomplete="first_name">
                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="LastName" class="form-label">Last Name of Employee</label>
                                    <input id="LastName" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ $employee['last_name'] }}" required autocomplete="last_name">
                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="CompanyID" class="form-label">Company of Employee</label>
                                    <select name="company" id="CompanyID" class="form-select form-control" required>
                                        <option value="">Select the Employee Company</option>
                                        @forelse($companies as $company)
                                            <option value="{{ $company->id }}" @if($employee['company'] == $company->id) selected @endif>{{ $company->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="EmailId" class="form-label">Email of Employee</label>
                                    <input id="EmailId" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $employee['email'] }}" autocomplete="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="PhoneNumber" class="form-label">Phone Number of Employee</label>
                                    <input id="PhoneNumber" type="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ $employee['phone'] }}" autocomplete="phone">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12 d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ route('employee.index') }}" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
