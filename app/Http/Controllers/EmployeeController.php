<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use App\Models\Employees;
use Crypt;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmployeeController extends Controller
{
    /**
     * Returning the Employees list.
     *
     * @return Application|Factory|View
     */
    public function index() {
        $employees = Employees::latest()->paginate(10);
        return view('pages.employees.index', compact('employees'));
    }

    /**
     * Returning Employee Create Form.
     *
     * @return Application|Factory|View
     */
    public function create() {
        $companies = Companies::all();
        return view('pages.employees.create', compact('companies'));
    }

    /**
     * Creating new employee in database.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request) {
        $data = $this->validate($request, [
            'first_name' => 'required|string|min:2',
            'last_name' => 'required|string|min:2',
            'company' => 'required|integer|exists:companies,id',
            'email' => 'nullable|email|unique:employees,email',
            'phone' => 'nullable|integer|digits:10'
        ]);
        $create = Employees::create($data);
        if ($create !== null) {
            return redirect()->route('employee.index')->with('success', 'Employee Created Successfully');
        }
        return redirect()->back()->with('error', 'Something Went Wrong');
    }

    /**
     * Showing Single Employee
     *
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id) {
        $id = $this->IdCrypt($id);
        $employee = Employees::find($id);
        return view('pages.employees.view', compact('employee'));
    }

    /**
     * Editing Page of Employee
     *
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id) {
        $id = $this->IdCrypt($id);
        $employee = Employees::find($id);
        $companies = Companies::all();
        return view('pages.employees.edit', compact('employee', 'companies'));
    }

    /**
     * Updating Employee Details.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id) {
        $id = $this->IdCrypt($id);
        $employee = Employees::find($id);
        $data = $this->validate($request, [
            'first_name' => 'required|string|min:2',
            'last_name' => 'required|string|min:2',
            'company' => 'required|integer|exists:companies,id',
            'email' => 'nullable|email',
            'phone' => 'nullable|integer|digits:10'
        ]);
        $update = $employee->update($data);
        if ($update !== true) {
            return redirect()->back()->with('error', 'Something Went Wrong');
        }
        return redirect()->route('employee.index')->with('success', 'Employee Updated Successfully');
    }

    /**
     * Deleting Employee from records
     *
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id) {
        $id = $this->IdCrypt($id);
        $employee = Employees::find($id);
        $delete = $employee->delete();
        if ($delete !== true) {
            return redirect()->back()->with('error', 'Something Went Wrong');
        }
        return redirect()->back()->with('success', 'Deleted Successfully');
    }

    /**
     * Decrypting ID using crypt
     *
     * @param $id
     * @return mixed
     */
    public function IdCrypt($id) {
        $id = htmlspecialchars(stripslashes($id));
        return Crypt::decrypt($id);
    }
}
