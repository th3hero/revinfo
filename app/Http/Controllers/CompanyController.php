<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use Crypt;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Image;
use Storage;

class CompanyController extends Controller
{
    /**
     * Displaying all companies available in database
     *
     * @return Application|Factory|View
     */
    public function index() {
        $companies = Companies::latest()->paginate(10);
        return view('pages.companies.index', compact('companies'));
    }

    /**
     * Rendering Create Company Form
     *
     * @return Application|Factory|View
     */
    public function create() {
        return view('pages.companies.create');
    }

    /**
     * Storing New Company Information in Database;
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request) {
        $data = $this->validate($request, [
            'name' => 'required|string',
            'email' => 'nullable|email|unique:companies,email',
            'logo' => 'nullable|image|mimes:jpeg,jpg,png,svg',
            'website' => 'nullable|url'
        ]);
        $name = null;
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $add = rand(1, 1000).$data['name'];
            $name = $add.$file->getClientOriginalName();
            Image::make($file->getRealPath())->resize('100', 100)->save(storage_path('app/public').$name, '100');
        }
        $create = Companies::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'logo' => $name,
            'website' => $data['website']
        ]);
        if ($create !== null) {
            return redirect()->route('company.index')->with('success', 'Company Created Successfully');
        }
        return redirect()->back()->with('error', 'Something Went Wrong');

    }

    /**
     * Viewing a single company details
     *
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id) {
        $id = $this->IdDecrypt($id);
        $company = Companies::find($id);
        return view('pages.companies.view', compact('company'));
    }

    /**
     * Returning Edit form for company editing
     *
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id) {
        $id = $this->IdDecrypt($id);
        $company = Companies::find($id);
        return view('pages.companies.edit', compact('company'));
    }

    /**
     * Updating the company records in database.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id) {
        $data = $this->validate($request, [
            'name' => 'required|string',
            'email' => 'nullable|email',
            'logo' => 'nullable|image|mimes:jpeg,jpg,png,svg',
            'website' => 'nullable|url'
        ]);
        $id = $this->IdDecrypt($id);
        $company = Companies::find($id);
        $logo = $company['logo'];
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $add = rand(1, 1000).$data['name'];
            $name = $add.$file->getClientOriginalName();
            Image::make($file->getRealPath())->resize('100', 100)->save(storage_path('app/public').$name, '100');
            $logo = $name;
        }
        $update = $company->update([
            'name' => $data['name'],
            'email' => $data['email'],
            'logo' => $logo,
            'website' => $data['website']
        ]);
        if ($update !== true) {
            return redirect()->back()->with('error', 'Something Went Wrong');
        }
        return redirect()->route('company.index')->with('success', 'Company Updated Successfully');
    }

    /**
     * Deleting the Company from database.
     *
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id) {
        $id = $this->IdDecrypt($id);
        $company = Companies::find($id);
        $status = $company->delete();
        if ($status !== true) {
            return redirect()->back()->with('error', 'Something Went Wrong');
        }
        return redirect()->back()->with('success', 'Deleted Successfully');
    }

    private function IdDecrypt($id) {
        $id = htmlspecialchars(stripslashes($id));
        return Crypt::decrypt($id);
    }
}
